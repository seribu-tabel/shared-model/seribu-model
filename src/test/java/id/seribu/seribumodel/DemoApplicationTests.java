package id.seribu.seribumodel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import id.seribu.seribumodel.enumeration.RoleEnum;
import id.seribu.seribumodel.user.MUser;

public class DemoApplicationTests {

	private MUser mUser;

	@Before
	public void setup(){
		mUser = new MUser();
		mUser.setRoles(RoleEnum.valueOf("ROLE_SUPER".toUpperCase()));
	}

	@Test
	public void testEnumeration(){
		System.out.println(mUser.getRoles());
		Assert.assertEquals(RoleEnum.ROLE_SUPER, mUser.getRoles());
	}

	@Test
	public void testGetEnumText(){
		String text = mUser.getRoles().name();
		Assert.assertEquals("ROLE_SUPER", text);
	}

	@Test
	public void testROleEnum(){
		Assert.assertEquals("ROLE_SUPER", RoleEnum.ROLE_SUPER.toString());
		RoleEnum[] arrayEnum = RoleEnum.values();
		for (RoleEnum num : arrayEnum) {
			System.out.println("value: "+ num.toString() + " text "+ num.getText());
		}
	}
}
