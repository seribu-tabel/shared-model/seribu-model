package id.seribu.seribumodel.transaction;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import lombok.Data;

@Data
@Entity
@Table(name = "t_kepatuhan_bendahara_status")
public class TKepatuhanBendaharaStatus implements Persistable<UUID> {

    @Id
    @Column(name = "id")
    private UUID id;
    @Column
	private String tahun;
    @Column(name = "kode_kpp")
    private String kodeKpp;
    @Column(name = "nama_kpp")
    private String namaKpp;
    @Column
    private String kanwil;
    @Column
    private String provinsi;
    @Column
    private String pulau;
    @Column
    private String jenis;
    @Column(name = "status_npwp")
    private String statusNpwp;
    @Column(name = "status_pkp")
    private String statusPkp;
    @Column
    private int jumlah;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_date")
	private Date createdDate;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return true;
    }

}