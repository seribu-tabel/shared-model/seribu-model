package id.seribu.seribumodel.transaction;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import lombok.Data;

@Data
@Entity
@Table(name = "t_penerimaan_op_wilayah_klu")
public class TPenerimaanOpWilayahKlu implements Persistable<UUID> {

    @Id
    @Column(name = "id")
    private UUID id;
    @Column
	private String tahun;
    @Column(name = "kode_kpp")
    private String kodeKpp;
    @Column(name = "nama_kpp")
    private String namaKpp;
    @Column
    private String kanwil;
    @Column
    private String provinsi;
    @Column
    private String pulau;
    @Column(name = "kode_kategori")
    private String kodeKategori;
    @Column(name = "nama_kategori")
    private String namaKategori;
    @Column
    private int jumlah;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_date")
	private Date createdDate;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return true;
    }

}