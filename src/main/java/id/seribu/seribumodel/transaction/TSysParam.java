package id.seribu.seribumodel.transaction;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name="t_sys_param")
@Entity
public class TSysParam implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column
    private String name;
    @Column
    private String value;
    @Column(name = "group_param")
    private String groupParam;
    @Column
    private String description;

}