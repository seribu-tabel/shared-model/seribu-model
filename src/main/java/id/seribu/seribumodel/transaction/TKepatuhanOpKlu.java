package id.seribu.seribumodel.transaction;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import lombok.Data;

@Data
@Entity
@Table(name = "t_kepatuhan_op_klu")
public class TKepatuhanOpKlu implements Persistable<UUID> {

    @Id
    @Column(name = "id")
    private UUID id;
    @Column
	private String tahun;
    @Column(name = "kode_kategori")
    private String kodeKategori;
    @Column(name = "nama_kategori")
    private String namaKategori;
    @Column(name = "status_perkawinan")
    private String statusPerkawinan;
    @Column(name = "jumlah_tanggungan")
    private String jumlahTanggungan;
    @Column(name = "jenis_op")
    private String jenisOp;
    @Column(name = "range_usia")
    private String rangeUsia;
    @Column(name = "jenis_kelamin")
    private String jenisKelamin;
    @Column(name = "warga_negara")
    private String wargaNegara;
    @Column
    private int jumlah;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_date")
	private Date createdDate;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return true;
    }

}