package id.seribu.seribumodel.transaction;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "t_import_file")
public class TImportFile implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column
    private UUID id;
    @Column
    private String dataset;
    @Column(name = "subyek_data")
    private String subyekData;
    @Column(name = "source_data")
    private String sourceData;
    @Column
    private String directory;
    @Column
    private String filename;
    @Column
    private boolean process;
    @Column(name = "uploaded_by")
    private String uploadedBy;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "upload_date")
	private Date uploadDate;
}