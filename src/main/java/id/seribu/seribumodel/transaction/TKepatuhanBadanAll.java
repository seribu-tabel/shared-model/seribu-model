package id.seribu.seribumodel.transaction;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

import lombok.Data;

@Data
@Entity
@Table(name = "t_kepatuhan_badan_all")
public class TKepatuhanBadanAll implements Persistable<UUID> {

    @Id
    @Column(name = "id")
    private UUID id;
    @Column
	private String tahun;
    @Column(name = "kode_kpp")
    private String kodeKpp;
    @Column(name = "nama_kpp")
    private String namaKpp;
    @Column
    private String kanwil;
    @Column
    private String provinsi;
    @Column
    private String pulau;
    @Column(name = "kode_kategori")
    private String kodeKategori;
    @Column(name = "nama_kategori")
    private String namaKategori;
    @Column(name = "kategori_wp")
    private String kategoriWp;
    @Column
    private String modal;
    @Column(name = "status_pusat")
    private String statusPusat;
    @Column(name = "periode_pembukuan")
    private String periodePembukuan;
    @Column(name = "tahun_pendirian")
    private String tahunPendirian;
    @Column(name = "badan_hukum")
    private String badanHukum;
    @Column
    private int jumlah;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_date")
	private Date createdDate;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return true;
    }

}