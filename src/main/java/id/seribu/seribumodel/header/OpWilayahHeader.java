package id.seribu.seribumodel.header;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "v_op_wilayah_header")
public class OpWilayahHeader implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column
    private String value;
    @Column
    private String text;
}