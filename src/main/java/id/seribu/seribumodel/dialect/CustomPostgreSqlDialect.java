package id.seribu.seribumodel.dialect;

import java.sql.Types;

import org.hibernate.dialect.PostgreSQL94Dialect;

/**
 * @author arieki
 */
public class CustomPostgreSqlDialect extends PostgreSQL94Dialect{

    private static final String JSONB = "jsonb";

    public CustomPostgreSqlDialect(){
        this.registerColumnType(Types.JAVA_OBJECT, JSONB);
    }
}