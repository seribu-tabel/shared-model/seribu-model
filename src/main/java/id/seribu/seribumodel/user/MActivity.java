package id.seribu.seribumodel.user;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Table(name = "m_activity_log")
@Entity
public class MActivity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;
    @Column
    private String username;
    @Column
    private String nip;
    @Column(name = "role")
    private String authority;
    @Column(name = "ip_address")
    private String ipAddress;
    @Column
    private String activity;
    @Column
    private String dataset;
    @Column(name = "subyek_data")
    private String subyekData;
    @Column(name = "data_source")
    private String sourceData;
    @Column(name = "first_character")
    private String firstCharacter;
    @Column(name = "second_character")
    private String secondCharacter;
    @Column(name = "access_date")
    private Date accessDate;
    @Column(name = "created_date")
    private Date createdDate;
}