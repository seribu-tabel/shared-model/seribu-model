package id.seribu.seribumodel.enumeration;

/**
 * @author arieki
 */
public enum StatusEnum{
    Y, 
    N,
    SUCCESS,
    INVALID,
    ERROR;
}
