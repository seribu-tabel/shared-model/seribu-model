package id.seribu.seribumodel.enumeration;

/**
 * @author arieki
 */
public enum RoleEnum{
    ROLE_SUPER("Super admin"),
    ROLE_ADMIN("Admin"),
    ROLE_USER("User");

    String text;
    RoleEnum() {}

    RoleEnum(String text) {
        this.text = text;
    }

    /**
     * @return the text
     */
    public String getText() {
        return this.text;
    }

}