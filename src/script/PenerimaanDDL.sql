CREATE TABLE t_penerimaan_op_wilayah (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	status_perkawinan varchar(100),
	jumlah_tanggungan varchar(50),
	jenis_op varchar(255),
	range_usia varchar(100),
	jenis_kelamin varchar(100),
	warga_negara varchar(100),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_op_wilayah_pk PRIMARY KEY (id)
);
-- Permissions
ALTER TABLE t_penerimaan_op_wilayah OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_op_wilayah TO seribu_trx;

CREATE TABLE t_penerimaan_op_wilayah_klu (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_op_wilayah_klu_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_op_wilayah_klu OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_op_wilayah_klu TO seribu_trx;

CREATE TABLE t_penerimaan_op_klu (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	status_perkawinan varchar(100),
	jumlah_tanggungan varchar(50),
	jenis_op varchar(255),
	range_usia varchar(100),
	jenis_kelamin varchar(100),
	warga_negara varchar(100),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_op_klu_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_op_klu OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_op_klu TO seribu_trx;

CREATE TABLE t_penerimaan_op_status (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	status_npwp varchar(50),
	status_pkp varchar(50),
	metode_pembukuan varchar(50),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_op_status_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_op_status OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_op_status TO seribu_trx;

CREATE TABLE t_penerimaan_badan_status (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	kategori_wp varchar(255),
	modal varchar(255),
	status_pusat varchar(255),
	periode_pembukuan varchar(255),
	tahun_pendirian varchar(255),
	badan_hukum varchar(255),
	status_npwp varchar(50),
	status_pkp varchar(50),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_badan_status_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_badan_status OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_badan_status TO seribu_trx;

CREATE TABLE t_penerimaan_badan_all (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	kategori_wp varchar(255),
	modal varchar(255),
	status_pusat varchar(255),
	periode_pembukuan varchar(255),
	tahun_pendirian varchar(255),
	badan_hukum varchar(255),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_badan_all_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_badan_all OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_badan_all TO seribu_trx;

CREATE TABLE t_penerimaan_bendahara_all (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	jenis varchar(255),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_bendahara_all_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_bendahara_all OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_bendahara_all TO seribu_trx;

CREATE TABLE t_penerimaan_bendahara_status (
	id uuid NOT NULL,
	tahun varchar(50),
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	jenis varchar(255),
	jumlah numeric,
	status_npwp varchar(255),
	status_pkp varchar(255),
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_bendahara_status_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_bendahara_status OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_bendahara_status TO seribu_trx;

CREATE TABLE t_penerimaan_umum_wilayah_klu (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	jenis_wp varchar(50),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_umum_wilayah_klu_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_umum_wilayah_klu OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_umum_wilayah_klu TO seribu_trx;

CREATE TABLE t_penerimaan_umum_status (
	id uuid NOT NULL,
	tahun varchar(50),
	kode_kpp varchar(50),
	nama_kpp varchar(255),
	kanwil varchar(255),
	provinsi varchar(255),
	pulau varchar(255),
	kode_kategori varchar(255),
	nama_kategori varchar(255),
	status_npwp varchar(255),
	status_pkp varchar(255),
	jenis_wp varchar(50),
	jumlah numeric,
	created_by varchar(50),
	created_date timestamp,
	CONSTRAINT t_penerimaan_umum_status_pk PRIMARY KEY (id)
);
ALTER TABLE t_penerimaan_umum_status OWNER TO seribu_trx;
GRANT ALL ON TABLE t_penerimaan_umum_status TO seribu_trx;