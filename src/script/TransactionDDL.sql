-- DDL SERIBU_TRX_DB
CREATE TABLE t_sys_param (
	"name" varchar(25) NOT NULL,
	value varchar(255) NOT NULL,
	group_param varchar(255) NOT NULL,
	description varchar(255) NULL,
	CONSTRAINT t_sys_param_un UNIQUE (name)
);
-- Permissions
ALTER TABLE t_sys_param OWNER TO seribu_trx;
GRANT ALL ON TABLE t_sys_param TO seribu_trx;

CREATE TABLE t_subdit (
	id uuid NOT NULL,
	subdit_name varchar(50) NOT NULL,
	path varchar(100) NOT NULL,
	"file_name" varchar(100) NOT NULL,
	file_type varchar(100) NOT NULL,
	uploaded_by varchar(50) NOT NULL,
	enabled bool NULL DEFAULT true,
	file_type varchar(100),
    release_year numeric,
	description varchar(255),
	created_date timestamp NOT NULL,
	updated_date timestamp NULL,
	CONSTRAINT t_subdit_pk PRIMARY KEY (id)
);
CREATE INDEX t_subdit_subdit_name_idx ON t_subdit USING btree (subdit_name, enabled);
CREATE INDEX t_subdit_file_name_idx ON t_subdit (file_name,enabled,subdit_name);
-- Permissions
ALTER TABLE t_subdit OWNER TO seribu_trx;
GRANT ALL ON TABLE t_subdit TO seribu_trx;

CREATE TABLE t_pihak_lain (
	id uuid NOT NULL,
	subdit_name varchar(50) NOT NULL,
	path varchar(100) NOT NULL,
	"file_name" varchar(100) NOT NULL,
	file_type varchar(100) NOT NULL,
	uploaded_by varchar(50) NOT NULL,
	enabled bool NULL DEFAULT true,
	file_type varchar(100),
	release_year numeric,
	description varchar(255),
	created_date timestamp NOT NULL,
	updated_date timestamp NULL,
	CONSTRAINT t_pihak_lain_pk PRIMARY KEY (id)
);
CREATE INDEX t_pihak_lain_subdit_name_idx ON t_pihak_lain USING btree (subdit_name, enabled);
CREATE INDEX t_pihak_lain_file_name_idx ON t_subdit (file_name,enabled,subdit_name);
-- Permissions
ALTER TABLE t_pihak_lain OWNER TO seribu_trx;
GRANT ALL ON TABLE t_pihak_lain TO seribu_trx;

CREATE TABLE t_import_file (
	id uuid NOT NULL,
	dataset varchar(255),
	subyek_data varchar(255),
	source_data varchar(255),
	directory varchar(255),
	filename varchar(255),
	process bool,
	uploaded_by varchar(50),
	updated_by varchar(50),
	upload_date timestamp,
	CONSTRAINT t_import_file_pk PRIMARY KEY (id)
);
ALTER TABLE t_import_file OWNER TO seribu_trx;
GRANT ALL ON TABLE t_import_file TO seribu_trx;


create or replace view v_badan_all_header as 
	select regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value,
	initcap(replace(column_name, '_', ' ')) as text
  	from information_schema.columns
 	where table_schema = 'seribu_trx'
   	and table_name = 't_demo_badan_all'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');
   
create or replace view v_badan_status_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_badan_status'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');
   
create or replace view v_bendahara_status_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_bendahara_status'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');
  
create or replace view v_bendahara_all_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_bendahara_all'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');

create or replace view v_op_wilayah_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_op_wilayah'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');

create or replace view v_op_wilayah_klu_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_op_wilayah_klu'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');
   
create or replace view v_op_klu_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_op_klu'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');
   
create or replace view v_op_status_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_op_status'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');

create or replace view v_umum_status_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_umum_status'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');
   	
create or replace view v_umum_wilayahklu_header as 
	SELECT regexp_replace(
			replace(initcap(columns.column_name), '_', ''), 
			'^['||upper(left(columns.column_name, 1))||']', 
			lower(left(columns.column_name, 1))) AS value, initcap(replace(column_name, '_', ' ')) as text
  	FROM information_schema.columns
 	WHERE table_schema = 'seribu_trx'
   	AND table_name   = 't_demo_umum_wilayah_klu'
   	and column_name not in ('id', 'created_by', 'created_date', 'jumlah');

insert into t_sys_param
(name, value, group_param, description)
values
('status', 'Status', 'database_op', 'list db of op');