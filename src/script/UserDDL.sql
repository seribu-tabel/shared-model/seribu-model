-- DDL SERIBU_USER_DB
CREATE TABLE seribu_user.m_user (
	id uuid NOT NULL,
	username varchar(50) NOT NULL,
	"name" varchar(255) NOT NULL,
	nip varchar(9) NOT NULL,
	"password" varchar(255) NOT NULL,
	created_date timestamp NOT NULL,
	updated_date timestamp NULL,
	"role" varchar(20) NOT NULL,
	enabled bool NULL DEFAULT true,
	CONSTRAINT m_user_pk PRIMARY KEY (id),
	CONSTRAINT m_user_un UNIQUE (username)
);
CREATE INDEX m_user_username_idx ON seribu_user.m_user USING btree (username, enabled, role);

-- Permissions
ALTER TABLE seribu_user.m_user OWNER TO seribu_user;
GRANT ALL ON TABLE seribu_user.m_user TO seribu_user;

CREATE TABLE seribu_user.m_activity_log(
	id uuid NOT NULL,
	username varchar(50) NOT NULL,
	nip varchar(9) NOT NULL,
	role varchar(20) NOT NULL,
	ip_address varchar(255),
	activity varchar(255),
	dataset varchar(255),
	subyek_data varchar(255),
	data_source varchar(255),
	first_character varchar(255),
	second_character varchar(255),
	access_date timestamp,
	created_date timestamp NOT NULL,
	CONSTRAINT m_activity_log_pk PRIMARY KEY (id)
);
-- Permissions
ALTER TABLE seribu_user.m_activity_log OWNER TO seribu_user;
GRANT ALL ON TABLE seribu_user.m_activity_log TO seribu_user;